#ifndef _GNSS_TYPES_H_
#define _GNSS_TYPES_H_

#include <syscfg/syscfg.h>


struct gnss;

struct gnss_event {
    struct os_event event;
    uint8_t type;
};

typedef struct gnss_event gnss_event_t;
typedef struct gnss gnss_t;

#define GNSS_FLOAT_INTEGER_SCALE 10000


#if   GNSS_FLOAT_INTEGER_SCALE == 1000
#define GNSS_FLOAT_INTEGER_NDIGITS 3
#elif GNSS_FLOAT_INTEGER_SCALE == 10000
#define GNSS_FLOAT_INTEGER_NDIGITS 4
#elif GNSS_FLOAT_INTEGER_SCALE == 100000
#define GNSS_FLOAT_INTEGER_NDIGITS 5
#else
#error "GNSS_FLOAT_INTEGER_SCALE value not supported"
#endif



#if   MYNEWT_VAL(GNSS_USE_FLOAT) == 2
typedef __fp16 gnss_float_t;
#define GNSS_FLOAT_0 0.0f
#define GNSS_SYSFLOAT(x) (x)
#elif MYNEWT_VAL(GNSS_USE_FLOAT) == 1
typedef float  gnss_float_t;
#define GNSS_FLOAT_0 0.0f
#define GNSS_SYSFLOAT(x) (x)
#elif MYNEWT_VAL(GNSS_USE_FLOAT) == 0
typedef long  gnss_float_t;
#define GNSS_FLOAT_0 0
#define GNSS_SYSFLOAT(x) (((float)(x)) / GNSS_FLOAT_INTEGER_SCALE)
#else
#error "Unknown float definition for GNSS_USE_FLOAT)"
#endif

typedef struct gnss_date {
    uint32_t year         : 16;
    uint32_t month        :  4;
    uint32_t day          :  5;
    uint32_t present      :  1;
} gnss_date_t;

typedef struct gnss_time {
    uint32_t hours        :  5;
    uint32_t minutes      :  6; 
    uint32_t seconds      :  6; 
    uint32_t microseconds : 10; 
    uint32_t present      :  1;
} gnss_time_t;

typedef struct gnss_sat_info {
    uint32_t prn          :  8;	    /**< Satellit PRN number             */
    uint32_t elevation    :  7;	    /**< Eleveation (degrees) (0-90)     */
    uint32_t azimuth      :  9;	    /**< Azimuth (True North degrees) (0-359) */
    uint32_t snr          :  7;     /**< SNR (dB) (0-99)                 */
} gnss_sat_info_t;


#endif
