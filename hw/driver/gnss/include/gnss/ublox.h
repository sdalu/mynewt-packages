#ifndef _GNSS_UBLOX_H_
#define _GNSS_UBLOX_H_

#define GNSS_UBLOCK_DEFAULT_BAUD_RATE 9600

/**
 * Configuration of the Ublox driver
 */
struct gnss_ublox {
    int wakeup_pin;       /**< pin use for wakeup             */
    int reset_pin;        /**< pin use for reset              */
    uint16_t cmd_delay;   /**< delay required after a command */

    int standby_level;
};

/**
 * Initialize the driver layer with Ublox device.
 *
 * @param ctx		GNSS context 
 * @param uart		Configuration
 *
 * @return true on success
 */
bool gnss_ublox_init(gnss_t *ctx, struct gnss_ublox *mtk);

bool gnss_ublox_standby(gnss_t *ctx, int level);
bool gnss_ublox_wakeup(gnss_t *ctx);
bool gnss_ublox_reset(gnss_t *ctx, int type);

#endif
