#include <gnss/gnss.h>
#include <gnss/log.h>

bool
gnss_nmea_decoder_vtg(struct gnss_nmea_vtg *vtg, char *field, int fid) {
    bool success = true;
    union {
	char type;
	char unit;
	gnss_float_t speed;
    } local;

    switch(fid) {
    case  0:	/* xxVTG */
	break;

    case  1:	/* True Track */
	success = gnss_nmea_field_parse_float(field, &vtg->true_track);
	break;	

    case  2: 	/* True (T) */
	success = gnss_nmea_field_parse_char(field, &local.type) &&
	          (local.type == 'T');
	break;

    case  3:	/* Magentic Track */
	success = gnss_nmea_field_parse_float(field, &vtg->magnetic_track);
	break;	

    case  4: 	/* Magnetic (M) */
	success = gnss_nmea_field_parse_char(field, &local.type) &&
	          (local.type == 'M');
	break;

    case  5: 	/* Speed */
	success = gnss_nmea_field_parse_float(field, &local.speed);
	if (success) {
	    vtg->speed = _gnss_nmea_knot_to_mps(local.speed);
	}
	break;

    case  6: 	/* Knots (N) */
	success = gnss_nmea_field_parse_char(field, &local.unit) &&
	          (local.unit == 'N');
	break;

    case  7: 	/* Speed */
	success = gnss_nmea_field_parse_float(field, &local.speed);
	if (success) {
	    if ((vtg->speed == GNSS_FLOAT_0) && (local.speed != GNSS_FLOAT_0)) {
		vtg->speed = _gnss_nmea_kmph_to_mps(local.speed);
	    }
	}
	break;

    case  8: 	/* Km/h (K) */
	success = gnss_nmea_field_parse_char(field, &local.unit) &&
	          (local.unit == 'K');
	break;

    case  9:	/* FAA mode */
	success = gnss_nmea_field_parse_char(field, &vtg->faa_mode);
	break;


    default:
	assert(0);
	break;
    }

    return success;
}

void
gnss_nmea_log_vtg(struct gnss_nmea_vtg *vtg)
{
    LOG_INFO(&_gnss_log, LOG_MODULE_DEFAULT,
	     "VTG: Track = %f°[T] | %f°[M], Speed = %f m/s, FAA = %c\n",
	     GNSS_SYSFLOAT(vtg->true_track),
	     GNSS_SYSFLOAT(vtg->magnetic_track),
	     GNSS_SYSFLOAT(vtg->speed),
	     vtg->faa_mode);
}
