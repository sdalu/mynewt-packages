#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <syscfg/syscfg.h>
#include <gnss/nmea.h>
#include <console/console.h>



bool
gnss_nmea_field_parse_char(const char *str, char *val)
{
    char v = *str;
    if (val) { *val = v; }
    return str[v ? 1 : 0] == '\0';
}

bool
gnss_nmea_field_parse_long(const char *str, long *val)
{
    char *eob;
    long v = strtol(str, &eob, 10);
    if (val) { *val = v; }
    return *eob == '\0';
}

bool
gnss_nmea_field_parse_crc(const char *str, uint8_t *val)
{
    char *eob;
    long v = strtoul(str, &eob, 16);
    if (v > 255) return false;
    if (val) { *val = v; }
    return *eob == '\0';
}

bool
gnss_nmea_field_parse_float(const char *str, gnss_float_t *val)
{
    char *eob;
#if MYNEWT_VAL(GNSS_USE_FLOAT) > 0
    float v = strtod(str, &eob);
    if (val) { *val = v; }
    return *eob == '\0';
#else
    long v = strtol(str, &eob, 10);
    long f = 0;
    
    switch(*eob) {
    case '\0': goto done;
    case '.' : str = eob + 1; break;
    default  : return false;
    }

    eob = (char *)(str + GNSS_FLOAT_INTEGER_NDIGITS);
    while ((str < eob) && isdigit(*str)) {
	f = (f * 10) + (*str++ - '0');
    }
    while (isdigit(*str)) {
	str++;
    }
    if (*str != '\0')
	return false;
    
 done:
    if (v > (LONG_MAX / GNSS_FLOAT_INTEGER_SCALE)) {
	v =  LONG_MAX;
    } else if (v < (LONG_MIN / GNSS_FLOAT_INTEGER_SCALE)) {
	v = LONG_MIN;
    } else {
	v = v * GNSS_FLOAT_INTEGER_SCALE + f;
    }

    *val = v;
    return true;
#endif
}

bool
gnss_nmea_field_parse_direction(const char *str, int8_t *val)
{
    int8_t v = 0;
    switch (str[0]) {
    case 'N': case 'E': v =  1; break;
    case 'S': case 'W': v = -1; break;
    case '\0': break;
    default: return false;
    }
    if (val) { *val = v; }
    return str[v ? 1 : 0] == '\0';
}

bool
gnss_nmea_field_parse_latlng(const char *str, gnss_float_t *val)
{
#if MYNEWT_VAL(GNSS_USE_FLOAT) > 0
    char *eob;
    float v = strtod(str, &eob);
    if (val) { int dd  = v / 100;
	       *val = dd + (v - dd) / 60.0f; }
    return *eob == '\0';    
#else
    if (!gnss_nmea_field_parse_float(str, val)) {
	return false;
    }
    *val  = (*val / (100 * GNSS_FLOAT_INTEGER_SCALE ))
	      * GNSS_FLOAT_INTEGER_SCALE
	  + (*val % (100 * GNSS_FLOAT_INTEGER_SCALE))/60;
    return true;
#endif
}


bool
gnss_nmea_field_parse_date(const char *str, gnss_date_t *val)
{
    char *eob;
    gnss_date_t v = { .present = *str != '\0' };
    unsigned long n = strtoul(str, &eob, 10);
    if (val) { v.year    = (n % 100) + MYNEWT_VAL(GNSS_NMEA_YEAR_OFFSET);
	       v.month   = (n / 100) % 100;
	       v.day     =  n / 10000;
	       *val = v; }
    return *eob == '\0';
}
bool
gnss_nmea_field_parse_time(const char *str, gnss_time_t *val)
{
    gnss_time_t v = { .present = *str != '\0' };
    char *eob;
    unsigned long a,b = 0;

    a = strtoul(str, &eob, 10);
    if (a > 240000) {
	return false;
    }
    switch(*eob) {
    case '\0': goto done;
    case '.' : str = eob + 1; break;
    default  : return false;
    }
    eob = (char *)(str + 6);
    while ((str < eob) && isdigit(*str)) {
	b = (b * 10) + (*str++ - '0');
    }
    while (isdigit(*str)) {
	str++;
    }
    if (*str != '\0')
	return false;

 done:
    if (val) {	
	v.seconds      =  a % 100;
	a /= 100;
	v.minutes      =  a % 100;
	v.hours        =  a / 100;
	v.microseconds = b;
	*val = v;
    }
    return true;
}
