#include <hal/hal_gpio.h>
#include <gnss/gnss.h>
#include <gnss/nmea.h>
#include <gnss/ublox.h>


bool
gnss_ublox_init(gnss_t *ctx, struct gnss_ublox *ubx) {
    ctx->driver.conf    = ubx;

    ctx->driver.standby = gnss_ublox_standby;
    ctx->driver.wakeup  = gnss_ublox_wakeup;
    ctx->driver.reset   = gnss_ublox_reset;

    return true;
}

bool
gnss_ublox_standby(gnss_t *ctx, int level)
{
    struct gnss_ublox *ubx = ctx->driver.conf;

    switch(level) {
    default:
	(void)ubx;
	return false;
    }

    ubx->standby_level = level;
    return true;
}

bool
gnss_ublox_wakeup(gnss_t *ctx)
{
    struct gnss_ublox *ubx = ctx->driver.conf;

    switch(ubx->standby_level) {
    default:
	(void)ubx;
	return false;
    }
    
    return true;
}

bool
gnss_ublox_reset(gnss_t *ctx, int type)
{
    struct gnss_ublox *ubx = ctx->driver.conf;
        
    switch(type) {
    default:
	(void)ubx;
	return false;
    }
    
    return true;
}
